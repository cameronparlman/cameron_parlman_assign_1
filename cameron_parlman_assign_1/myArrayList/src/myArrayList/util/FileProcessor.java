package myArrayList.util ;



import java.io.File;
import java.util.Scanner;

import java.nio.file.Paths;
import java.nio.file.Files;
import java.util.stream.Stream;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;


public class FileProcessor{
	File inFile;
	Scanner scanner;
	
	public FileProcessor(String fileName){
		System.out.println("FileProcessor: constructor");
		try{
			inFile = new File(fileName);
			scanner = new Scanner(inFile);
		}catch(FileNotFoundException e){

		}
	}
		
//	MyArrayList arrayList= new MyArrayList();

	public int readLine(){
		if(scanner.hasNextInt()){
			return scanner.nextInt();
		}	
		return -1;
	}
}
