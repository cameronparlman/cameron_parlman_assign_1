/** 
command line arguments from huffman Big Java 6th edition
*/
package myArrayList.driver;
import myArrayList.util.FileProcessor;
import myArrayList.MyArrayList;

//import myArrayList.MyArrayList;

public class Driver 
{
	public static void main(String[] args) 
	{
		MyArrayList arrayList = new MyArrayList();	
		String inFile ="";
		String outFile = "";
			
	    // command line validation is missing here. FIXME!
		if(args.length != 3){ usage();}
		else{
			inFile = args[0];
			outFile = args[1];
		}
				
		System.out.println("Hello World: " + args[0] + ", " + args[1] + ", " + args[2]);//REM command line test 
		System.out.println("command lines: " + inFile + ", " + outFile );//REM command line test
		


		//fill myArraylist with info	
		FileProcessor fileProcessor = new FileProcessor(inFile);
		int line=-1;
		do{
			line = fileProcessor.readLine();
			if(line != -1)
			arrayList.insertSorted(line);
		}while(line > -1);
		
		

		//TESTER
		MyArrayListTest test = new MyArrayListTest();
		//RESULTS
		Results results = new Results();
		//call tester	
		test.testMe(arrayList, results);		


		
	}
	
	//need to call 


	/**
		Prints message describing proper usage.
	*/
	public static void usage(){
		System.out.println("Usage: java myArrayList infile outfile");	
	}
}
