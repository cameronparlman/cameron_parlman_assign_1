package myArrayList;
import java.util.Arrays;

public class MyArrayList{
	private int[] array = new int[50];
	private int first;
	private int last;
	private int size;
	public int length;
	private int numElements;		
	private boolean fragmented;
	
	/**
		This method should insert a new value but keep the data structure sorted in ascending order. 
		If a value is a duplicate, you can store it before or after an existing value. 
	*/
	public void insertSorted(int newValue){
		if(last == array.length){ resize();}
		int i = last;
		//System.out.println("new value: " + newValue + "i:" + i + " last:" + last);
		while(( i > first) && newValue < array[i-1]){
			array[i] = array[i-1];
			i--;
		}	
		array[i] = newValue;
		first =0;
		last++;
	}

	
	/** 
		this method should remove all occurences of a value, if it exists, and then move the remaining values 
		so that the array list has all values in ascending order. You can return an error code from this method
		(change return from void to int) if it helps your design.	
	*/
	public void removeValue(int value){/**TODO*/
		int i = 0;
		while(i < array.length && array[i] != value){
			i++;
		}
	
		array[i] = -1;
		
		numElements--;
	}


	/**
		This method should return the index of the first occurrence a value. It should return -1 if the value 
		does not exist in the array list.
	*/
	public int indexOf(int value){/**TODO*/
		int i = 0 ; 
		while(array[i] != value && i < array.length-1){i++;}
		if(array[i] == value){ return i;}	
		return -1;	
	}

	/**
		This method should return the total number of values that are stored in the array list
	*/
	public int size(){/**TODO*/return last;}
			

	/**
		This method should return the sum of all values that are stored in the array list.	
	*/
	public int sum(){/**TODO*/return -1;}
	

	/**
		method that prints all the values of the array in a pretty manner.
	*/
	public String toString(){
		/**TODO*/
		//check if fragmented. 
		if(fragmented){defrag();}
		int[] outArray = Arrays.copyOfRange(array, first, last);
		return Arrays.toString(outArray);
	}


	/**
		resize 1.5 times 	
	*/
	public void resize(){
		array = Arrays.copyOf(array, (int)(array.length*1.5));	
		Arrays.fill(array, last, array.length, -1);
		length = array.length;
	}	


	/**
		insert
	*/
	public void insert(int n){
			array[last] = n;
			last++;
			numElements++;
	}
	
	/**
		DEFRAGMENT ? 
	*/
	public void defrag(){
		int i = first;
		while(i < last){
			if(array[i] == -1){
				int j = i;
				while(j < last && j < array.length){
					array[j] = array[j+1]; 
					j++;
				}
				last--;
			}	
			i++;
		}	
		
			
		
	//assign new last
	}

	public int last(){
		int i = 0;
		while(array[i] != -1){
			i++;
		}
		return i;
	}


	/**
		ISSORTED?
	*/


	//TODO

	/**
		An empty constructor, which sets the privata data members to default values.		
	*/
	public MyArrayList(){
		last = first = 0;	
		size = 0;
		fragmented = false;
		numElements = 0;
		try{
		Arrays.fill(array, -1);	
		}catch(NullPointerException e){}
	}

}
