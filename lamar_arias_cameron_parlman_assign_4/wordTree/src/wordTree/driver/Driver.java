package wordTree.driver;
import wordTree.util.FileProcessor;
import wordTree.threadMgmt.Treebuilder;
import wordTree.threadMgmt.CreateWorkers;
import wordTree.store.Results;
import wordTree.util.MyLogger;
//import airportSecurityState.airportStates.Context;
import java.lang.System;
import java.io.IOException;

public class Driver{
 
 //run command ant -buildfile src/build.xml run -Darg0=src/input.txt -Darg1=src/output.txt -Darg2=1 -Darg3=banana -Darg4=0
 public static void main(String [] args){
 	try{
 		Integer.parseInt(args[2]);//number of threads
 		Integer.parseInt(args[4]);//debug level
 	}catch(NumberFormatException b){
        	System.err.println("number not given for number of threads or  debug level.");
        	b.printStackTrace();
        	System.exit(1);
        }
 	String wdelim = "[,]+";
 	String ldelim = "[ ]+";
 	
 	String [] wds = args[3].split(wdelim); 
 	Treebuilder tree = new Treebuilder();
 	FileProcessor inp = new FileProcessor();
 	inp.op(args[0]);
 	String line;
 	while((line = inp.re()) != null){
 		String [] w = line.split(ldelim);
 		for(int i = 0; i < w.length; i++){
 			tree.insertS(w[i]);
 		}
 	}
 	inp.cl();
 	for(int b = 0; b < wds.length; b++){
 	//	tree.delete(wds[b]);
 	}
 	
 	Results out = new Results();
 	CreateWorkers creator = new CreateWorkers(inp, out);
 	out = tree.printNodes(out, tree.root);
 	out.writeToFile(args[1]);
 
 }
 
 
 }
