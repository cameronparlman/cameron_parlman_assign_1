package wordTree.threadMgmt;
import java.util.ArrayList;
import wordTree.threadMgmt.Node;

import wordTree.store.Results;
 public class Treebuilder{
 	public Node root;
 	
 	public Treebuilder(){
 		root = null;
 	}
 	//makes the backup trees
 	//insert method for main tree that takes in the bnum and class to be added
 	public void insertS( String wd){
 		root = insertSR(root, wd);
 	}
 	//recursive insert method that inserts node into the main tree but also creates the clones of the node and inserts those clones into the backup trees. takes in the start node, bnum, and class to be added 
 	public Node insertSR(Node root, String wd){
 		if(root == null){
 			root = new Node(wd);
 			
 			
 			return root;
 		}
 		if(wd.equals(root.word))root.wcount += 1;
 		else if(wd.length() <= root.word.length())root.left = insertSR(root.left, wd);
 		else if(wd.length() > root.word.length())root.right = insertSR(root.right, wd);
 		//else if(num == root.bnum){
 			
 		//}
 		return root;
 	}
 	//insert function for backup trees. Takes in the bnum, class, and Node reference for the clone of the node to be inserted
 	
 	//function to search for node based on root and bnumber
 	/*
 	public Node search(Node root,String wd){
 		if(root == null || root.bnum == num)return root;
 		
 		if(root.bnum > num)return search(root.left, num);
 		
 		return search(root.right, num);
 	}
 	*/
 	//delete function to delete which takes the bnum and class to delete. calls recursive delete function
 	public void delete(String c){
 		root = deleteR(root, c);
 	}
 	//recursive delete function takes starting node, and the word to delete
 	//the word isnt deleted so much as the count for the word becomes 0
 	
 	public Node deleteR(Node root, String c){
 		//if(root.word == null)return root;
 		
 		if(root.word.equals(c)){
 			root.wcount = 0;
 			return root;
 		}
 		if(root.left != null && root.word.length() >= c.length()){
 			return deleteR(root.left, c);
 		}
 		if(root.right != null && root.word.length() < c.length()){
 			return deleteR(root.right, c);
 		}
 		return null;
 	}
 	
 	//adds to the results string arraylist for final output and traverses in order
 	public Results printNodes(Results out, Node root){
 		if(root.left != null){
 			out = printNodes(out, root.left);
 		}
 		//used for formatting the string to add to the array
 		//String p = word; 
 		//p = p +":";
 		if(root.wcount > 0){
 			out.output.add(root.word);
		}
 		if(root.right != null){
 		out = printNodes(out, root.right);
 		}
 		return out;
 	}
 	//counts the number of words in the tree
 	public int countW( Node root){
 		int Ws = root.wcount;
 		if(root.left != null){
 			Ws += countW( root.left);
 		}
 		
 		//used for formatting the string to add to the array
 		//String p = word; 
 		//p = p +":";
 		
 		//out.output.add(p);

 		if(root.right != null){
 			Ws = countW(root.right);
 		}
 		return Ws;
 	}
 	//gets number of distinct words
 	public int countDW( Node root){
 		int Ws = 0;
 		if(root.wcount != 0)Ws +=1;
 		if(root.left != null){
 			Ws += countW( root.left);
 		}
 		
 		//used for formatting the string to add to the array
 		//String p = word; 
 		//p = p +":";
 		
 		//out.output.add(p);

 		if(root.right != null){
 			Ws = countW(root.right);
 		}
 		return Ws;
 	}
 	//counts number of characters not including spaces
 	public int countChar( Node root){
 		int chars = root.word.length() * root.wcount;
 		if(root.left != null){
 			chars += countW( root.left);
 		}
 		
 		//used for formatting the string to add to the array
 		//String p = word; 
 		//p = p +":";
 		
 		//out.output.add(p);

 		if(root.right != null){
 			chars = countW(root.right);
 		}
 		return chars;
 	}
 	
 	
 	
 	
 	
 
 }
