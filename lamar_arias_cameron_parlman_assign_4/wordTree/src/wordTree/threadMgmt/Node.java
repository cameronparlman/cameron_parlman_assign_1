package wordTree.threadMgmt;
import java.util.ArrayList;

public class Node implements Cloneable{
	//word inserted to node
	public String word;
	//number of times word encountered
	public int wcount;
	//number of characters
	public int ccount;
	public Node left, right;
	
	//public ArrayList <Node> obsL;
	
	//public SubjectI sub;
	
	public Node( String wd){
		word = wd;
		wcount = 1;
		ccount = wd.length();
		left = null;
		right = null;
		
	}
	//makes a copy of the node and the important parameters not including the sub and observers when used
	/*
	public Node clone(){
		//Node tmp = new Node(bnum,course.get(0));
		//tmp.course = (ArrayList<String>)course.clone();
		
		return tmp;
	}
	*/
	
}
